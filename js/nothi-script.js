(function () {

  $('.btn_list').click(function (e) {
    e.preventDefault();
    $(this).addClass('active').siblings('button').removeClass('active');
    $('.tab-pane.active .list_items').show();
    $('.tab-pane.active .grid_items').hide();
  })
  $('.btn_grid').click(function (e) {
    e.preventDefault();
    $(this).addClass('active').siblings('button').removeClass('active');
    $('.tab-pane.active .list_items').hide();
    $('.tab-pane.active .grid_items').show();
  })


  $('.nothi_note tbody tr').click(function (e) {
    e.preventDefault();
    $(this).siblings().removeClass('active');
    $(this).toggleClass('active');
    $('.show_notes').toggleClass('showing');
    e.stopPropagation();
  })

  $('.note_cancel').click(function (e) {
    e.stopPropagation();
    $('.nothi_note tbody tr').removeClass('active');
    $('.show_notes').removeClass('showing');
  })

  $('.show_notes').click(function (e) {
    e.stopPropagation();
  })
  $(document).click(function (e) {
    e.stopPropagation();
    $('.nothi_note tbody tr').removeClass('active');
    $('.show_notes').removeClass('showing');
  })

  /*$('.daak_sender_btn').on('click', function (e) {
    $('.daak_sender_info').slideToggle(100);
    e.stopPropagation();
  })
  $(".daak_sender_info").click(function (e) {
    e.stopPropagation();
  });

  $(document).click(function () {
    $(".daak_sender_info").slideUp(100);
  });*/


  $(document).off('click', ".btn_search_officer").on('click', ".btn_search_officer", function (e) {
    e.preventDefault();
    $('.search_officer').show();
    $('.btn_group').hide();
  })


  $('.decision_list .btn-info').click(function (e) {
    e.preventDefault();
    $(this).parents('.list-group-item').siblings().removeClass('editable');
    $(this).parents('.list-group-item').toggleClass('editable');
    var msg = $(this).parents('.list-group-item').find('span.msg').text();
    $(this).parents('.list-group-item').find('input').val(msg).focus().select();
  })
  $('.decision_list .btn-success').click(function (e) {
    e.preventDefault();
    var msg = $(this).parents('.list-group-item').find('input').val();
    $(this).parents('.list-group-item').find('span.msg').text(msg)
    $(this).parents('.list-group-item').toggleClass('editable');
  })

  $('.list-group-item').find('input').on('keyup', function (e) {
    if (e.keyCode == 13) {
      var msg = $(this).parents('.list-group-item').find('input').val();
      $(this).parents('.list-group-item').find('span.msg').text(msg)
      $(this).parents('.list-group-item').toggleClass('editable');
    }
  });

  $('.decision_list .btn-danger').click(function (e) {
    e.preventDefault();
    if (confirm('If you press OK, this item will be removed permanently. Are you sure to remove this?') == true) {
      $(this).parents('.list-group-item').remove();
    }
  })


  // $('.toggle-data').html();
  $('.edit-nothi-btn').click(function (e) {
    e.preventDefault();
    $('.search_officer').show();
    $('.btn_group').hide();
    $('.add-nothi-btn').toggleClass('btn-success').toggleClass('btn-light');
    $(this).parents('.toggle-data').find('.list-data,.add-nothi-btn i,.add-nothi-btn span').toggleClass('showing ');
  });


  // $('.toggle-data').html();
  $('.add-nothi-btn,.cancel').click(function (e) {
    e.preventDefault();
    $('.search_officer').slideUp();
    $('.btn_group').slideDown();
    $('.add-nothi-btn').toggleClass('btn-success').toggleClass('btn-light');
    $(this).parents('.toggle-data').find('.list-data,.add-nothi-btn i,.add-nothi-btn span').toggleClass('showing ');
  })
  if ($('.datatable table').length) {
    $('.datatable table').DataTable();
  }

  if ($('.datatable-scroll table').length) {
    $('.datatable-scroll table').DataTable({
      dom: "<'row'<'col-sm-12 col-md-6'<'p-3'l>><'col-sm-12 col-md-6'<'p-3'f>>>" +
        "<'row'<'col-sm-12'<'pScroll'tr>>>" +
        "<'row'<'col-sm-12 col-md-5'<'p-3'i>><'col-sm-12 col-md-7'<'p-3'p>>>",
    });
  }


  $('.search_sender .dropdown-item').click(function (e) {
    e.preventDefault();
  })
  $('#adv_search').click(function (e) {
    $(this).hide();
    $('.search_sender .hide').toggleClass('show');
  })

  $('#adv_search_cancel').click(function (e) {
    $('#adv_search').show();
    $('.search_sender .hide').toggleClass('show');
  })


  $('.daak_forward').click(function (e) {
    e.preventDefault();


    $('.daak_btns').slideToggle(100);
    $('.daak_movement').slideToggle(100);

    $('html, body').animate({
      scrollTop: $("div.daak_movement").offset().top
    }, 1000)
    return false;
  })


  $('.pScroll').each(function () {
    const ps = new PerfectScrollbar($(this)[0], {
      useBothWheelAxes: true,
      wheelPropagation: true
    });
  });
  $('.datatable tbody').each(function () {
    const ps = new PerfectScrollbar($(this)[0], {
      useBothWheelAxes: true,
      wheelPropagation: true
    });
  });

  $(document).off('click', ".fa-close").on('click', ".fa-close", function () {
    $(this).closest('.up_item').remove();
  })

  var imagesPreview = function (input, placeToInsertImagePreview) {

    if (input.files) {

      for (i = 0; i < input.files.length; i++) {
        var reader = new FileReader();
        var extension = input.files[i].name.replace(/^.*\./, '');

        if (extension == 'docx' || extension == 'xlsx' || extension == 'xls' || extension == 'doc') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="docx up_item mb-2 mr-2"><i class="fa fa-close"></i><i class="fa fa-file-word-o"></i> </div>'
            );
          }
        }

        if (extension == 'pdf') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="pdf up_item mb-2 mr-2"><i class="fa fa-close"></i><i class="fa fa-file-pdf-o"></i> </div>'
            );
          }
        }

        if (extension == 'zip') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="zip up_item mb-2 mr-2"><i class="fa fa-close"></i><i class="fa fa-file-zip-o"></i> </div>'
            );
          }
        }

        if (extension == 'png' || extension == 'jpg' || extension == 'jpeg' || extension == 'svg') {
          reader.onload = function (event) {
            $(placeToInsertImagePreview).append(
              '<div class="image up_item mb-2 mr-2"><i class="fa fa-close"></i><img src="' + event.target.result + '" /> </div>'
            );
          }
        }


        reader.readAsDataURL(input.files[i]);
      }
    }

  };

  $('.file_uploader').on('change', function () {
    imagesPreview(this, 'div.file_uploader_gallery div.d-flex');
  });


  $('#mulpotro_upload').on('change', function () {
    imagesPreview(this, 'div.mulpotro_upload_gallery div.d-flex');
  });
  $('#shonjukti_upload').on('change', function () {
    imagesPreview(this, 'div.shonjukti_upload_gallery div.d-flex');
  });


  $('.daptorik_next').click(function () {
    $('.daptorik_1').slideUp(200);
    $('.daptorik_2').slideDown(200);
    $('.daptorik_back,.daptorik_save').show(200);
    $(this).hide();
  })
  $('.daptorik_back').click(function () {
    $('.daptorik_1').slideDown(200);
    $('.daptorik_2').slideUp(200);
    $('.daptorik_next').show(200);
    $('.daptorik_save').hide(200);
    $(this).hide();
  })

  $('.add_receiver .btn').click(function () {
    $('.popup_input').removeClass('hide');
  })
  $('.popup_input').click(function () {
    $(this).addClass('hide');
  })
  $('.popup_input input').click(function (e) {
    e.stopPropagation();
  })

  $('.sender_list li').each(function (e) {
    $(this).find('.btn_main').click(function (e) {
      $('.sender_list').addClass('main_selected');
      $(this).parents('li').addClass('has_main');
    })
    $('.label_main').click(function (e) {
      $('.sender_list').removeClass('main_selected');
      $(this).parents('li').removeClass('has_main');
    })

    $(this).find('.btn_copy').click(function (e) {
      $(this).parents('li').addClass('has_copy');
    })

    $('.label_copy').click(function (e) {
      $(this).parents('li').removeClass('has_copy');
    })
  })

  // $('.sender_list li');

  $('.daak_sender_btn').on('click', function (e) {
    $('.daak_sender_info').slideToggle(100);
    e.stopPropagation();
  })
  $(".daak_sender_info").click(function (e) {
    e.stopPropagation();
  });

  $(document).click(function () {
    $(".daak_sender_info").slideUp(100);
  });


/*
  $('#login_form').on('submit', function (e) {
    if ($('[name="username"]').val() == '' && $('[name="password"]').val() == '') {
      $('.alert').show();
      $('#alert_msg').text('User or pass can not be blank');
      return false;
    } else if ($('[name="username"]').val() == 'admin' && $('[name="password"]').val() == 'abc123') {
      $(this).submit();
    } else {
      $('.alert').show();
      $('#alert_msg').text('User or pass not matched.');
      return false;
    }
  });
*/

  $('.has-child i.toggler').on('click', function (e) {
    e.preventDefault();
    $(this).siblings('.nav-child').slideToggle(100);
  })


  $("#collapse-custom").click(function (e) {
    $(".collapse-custom").slideToggle(100);
    e.stopPropagation();
  });

  $(".collapse-custom").click(function (e) {
    e.preventDefault();
  });

  $('.adv-search-cancel').click(function () {
    $(".collapse-custom").slideUp(100);
    e.stopPropagation();
  });


  $('.toggle-input').click(function () {
    $(this).find('span').toggleClass('hidden');
    $('.login-reset').toggleClass('hidden');
    $('.login-login').toggleClass('hidden');
  })

  $('[data-toggle="tooltip"]').tooltip();

  if ($('#advanced-search').length) {
    $('#advanced-search').autoComplete({
      minChars: 1,
      source: function (term, suggest) {
        term = term.toLowerCase();
        var choices = ['প্রতিবেদন', 'টেমপ্লেট', 'জলবায়ু পরিবর্তন', 'পরিদর্শন প্রতিবেদন', 'ডাক', 'নথি', 'অধিশাখা'];
        var suggestions = [];
        for (i = 0; i < choices.length; i++)
          if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
        suggest(suggestions);
      }
    });
  }
  if ($('select.custom-select').length) {
    $('select.custom-select').select2();
  }

  if ($('select.select2').length) {
    $('select.select2').select2();
  }

  if ($('input.datepicker').length) {

    $('input.datepicker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true
    });
  }
  if ($('input.daterange-picker').length) {

    $('input.daterange-picker').daterangepicker({
      showDropdowns: true,
      opens: 'left'
    });
  }

  if ($('#daterange-menu').length) {


    var start = moment().subtract(29, 'দিন');
    var end = moment();

    function cb(start, end) {
      $('#daterange-menu span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('#daterange-menu').daterangepicker({
      startDate: start,
      endDate: end,
      ranges: {
        'আজ': [moment(), moment()],
        'গতকাল': [moment().subtract(1, 'দিন'), moment().subtract(1, 'দিন')],
        'গত ৭ দিন': [moment().subtract(6, 'দিন'), moment()],
        'শেষ ৩০ দিন': [moment().subtract(29, 'দিন'), moment()],
        'এই মাসে': [moment().startOf('মাস'), moment().endOf('মাস')],
        'গত মাসে': [moment().subtract(1, 'মাস').startOf('মাস'), moment().subtract(1, 'মাস').endOf('মাস')]
      }
    }, cb);

    cb(start, end);

  }

})(jQuery)
